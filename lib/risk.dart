import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RiskWidget extends StatefulWidget {
  RiskWidget({Key? key}) : super(key: key);

  @override
  _RiskWidgetState createState() => _RiskWidgetState();
}

class _RiskWidgetState extends State<RiskWidget> {
  var riskList = [
    'โรคทางเดินหายใจเรื้อรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรื้อรัง',
    'โรคหลอดเลือดสมอง',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน',
  ];
  var riskVals = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];

  @override
  void initState() { 
    super.initState();
    _loadRisk();
  }
  Future<void> _saveRisk() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('risk_values', riskVals.toString());
  }
  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskVal = prefs.getString('risk_values')??
    '[false, false, false, false, false, false, false]';
    var arrStrRiskVals = strRiskVal.substring(1, strRiskVal.length-1).split(',');
    setState(() {
      for(var i=0;i<arrStrRiskVals.length;i++){
        riskVals[i] = (arrStrRiskVals[i].trim() == 'true');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Risk'),),
      body: ListView(
        children: [
          CheckboxListTile(value: riskVals[0],
          title: Text(riskList[0]), 
          onChanged: (newVal){
            setState(() {
              riskVals[0] = newVal!;
            });
          }),
          CheckboxListTile(value: riskVals[1],
          title: Text(riskList[1]), 
          onChanged: (newVal){
            setState(() {
              riskVals[1] = newVal!;
            });
          }),
          CheckboxListTile(value: riskVals[2],
          title: Text(riskList[2]), 
          onChanged: (newVal){
            setState(() {
              riskVals[2] = newVal!;
            });
          }),
          CheckboxListTile(value: riskVals[3],
          title: Text(riskList[3]), 
          onChanged: (newVal){
            setState(() {
              riskVals[3] = newVal!;
            });
          }),
          CheckboxListTile(value: riskVals[4],
          title: Text(riskList[4]), 
          onChanged: (newVal){
            setState(() {
              riskVals[4] = newVal!;
            });
          }),
          CheckboxListTile(value: riskVals[5],
          title: Text(riskList[5]), 
          onChanged: (newVal){
            setState(() {
              riskVals[5] = newVal!;
            });
          }),
          CheckboxListTile(value: riskVals[6],
          title: Text(riskList[6]), 
          onChanged: (newVal){
            setState(() {
              riskVals[6] = newVal!;
            });
          }),
          ElevatedButton(
            onPressed: () async {
              await _saveRisk();
              Navigator.pop(context);
            },
            child: const Text('Save'))
        ],
      ),
    );
  }
}
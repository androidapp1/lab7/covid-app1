import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var questions = [
    'มีไข้หรือหนาวสั่น',
    'มีอาการไอ',
    'มีอาการแน่นหน้าอก',
    'มีอาการเหนื่อยล้า',
    'ปวดกล้ามเนื้อหรือร่างกาย',
    'ปวดหัว',
    'ไม่ได้กลิ่นและรส',
    'เจ็บคอ',
    'คัดจมูกน้ำมูกไหล',
    'คลื่นไส้อาเจียน',
    'ท้องเสีย',
  ];
  var questionVals = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];

 @override
  void initState() { 
    super.initState();
    _loadQuestion();
  }
  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', questionVals.toString());
  }
  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionVal = prefs.getString('question_values')??
    '[false, false, false, false, false, false, false, false, false, false, false]';
    var arrStrQuestionVals = strQuestionVal.substring(1, strQuestionVal.length-1).split(",");
    setState(() {
      for(var i=0;i<arrStrQuestionVals.length;i++){
        questionVals[i] = (arrStrQuestionVals[i].trim() == 'true');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Question'),),
      body: ListView(
        children: [
          CheckboxListTile(
            value: questionVals[0],
            title: Text(questions[0]),
            onChanged: (newVal){
              setState(() {
                questionVals[0] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[1],
            title: Text(questions[1]),
            onChanged: (newVal){
              setState(() {
                questionVals[1] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[2],
            title: Text(questions[2]),
            onChanged: (newVal){
              setState(() {
                questionVals[2] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[3],
            title: Text(questions[3]),
            onChanged: (newVal){
              setState(() {
                questionVals[3] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[4],
            title: Text(questions[4]),
            onChanged: (newVal){
              setState(() {
                questionVals[4] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[5],
            title: Text(questions[5]),
            onChanged: (newVal){
              setState(() {
                questionVals[5] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[6],
            title: Text(questions[6]),
            onChanged: (newVal){
              setState(() {
                questionVals[6] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[7],
            title: Text(questions[7]),
            onChanged: (newVal){
              setState(() {
                questionVals[7] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[8],
            title: Text(questions[8]),
            onChanged: (newVal){
              setState(() {
                questionVals[8] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[9],
            title: Text(questions[9]),
            onChanged: (newVal){
              setState(() {
                questionVals[9] = newVal!;
              });
            }
          ),
          CheckboxListTile(
            value: questionVals[10],
            title: Text(questions[10]),
            onChanged: (newVal){
              setState(() {
                questionVals[10] = newVal!;
              });
            }
          ),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: const Text('Save')
          )
        ],
      ),
    );
  }

}
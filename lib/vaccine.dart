import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class VaccineWidget extends StatefulWidget {
  VaccineWidget({Key? key}) : super(key: key);

  @override
  _VaccineWidgetState createState() => _VaccineWidgetState();
}

class _VaccineWidgetState extends State<VaccineWidget> {
  var vaccines = ['-','-','-'];

  Widget _vaccineComboBox({required String title, required String value, ValueChanged<String?>? onChanged}) {
    return Row(
            children: [
              Text(title),
              Expanded(child: Container()),
              DropdownButton(
                value: value,
                items: [
                  DropdownMenuItem(child: Text('-'), value: '-',),
                  DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer',),
                  DropdownMenuItem(child: Text('Jonhson & Jonhson'), value: 'Jonhson & Jonhson',),
                  DropdownMenuItem(child: Text('Spunik V'), value: 'Spunik V',),
                  DropdownMenuItem(child: Text('AstraZeneca'), value: 'AstraZeneca',),
                  DropdownMenuItem(child: Text('Novavax'), value: 'Novavax',),
                  DropdownMenuItem(child: Text('Sinopharm'), value: 'Sinopharm',),
                  DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac',),
                ],
                onChanged: onChanged,
              )
            ],
          );
  }

  @override
  void initState() { 
    super.initState();
    _loadVaccine();
  }

  Future<void> _loadVaccine() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      vaccines = prefs.getStringList('vaccines') ?? ['-','-','-'];
    });
  }

  Future<void> _saveVaccine() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setStringList('vaccines',vaccines);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Vaccine'),),
      body: Container(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: [
            _vaccineComboBox(title: 'เข็ม 1', value: vaccines[0], onChanged: (String? newVal){
              setState(() {
                vaccines[0] = newVal!;
              });
            }),
            _vaccineComboBox(title: 'เข็ม 2', value: vaccines[1], onChanged: (String? newVal){
              setState(() {
                vaccines[1] = newVal!;
              });
            }),
            _vaccineComboBox(title: 'เข็ม 3', value: vaccines[2], onChanged: (String? newVal){
              setState(() {
                vaccines[2] = newVal!;
              });
            }),
            ElevatedButton(
              onPressed: (){
                _saveVaccine();
                Navigator.pop(context);
              },
              child: const Text('Save')
            )
          ],
        ),
      )
    );
  }
}